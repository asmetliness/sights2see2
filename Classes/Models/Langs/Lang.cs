namespace GidTaxiApi.Classes.Models.Langs
{
    /// <summary>
    /// Язык (английский, русский, и т.п.)
    /// </summary>
    public class Lang
    {
        public int Id { get; set; }
        public string Ident { get; set; }
        public string Name { get; set; }
    }
}