﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GidTaxiApi.Classes.Models.Promos
{
    public class Promo
    {
        public string Uid { get; set; }
        public string ClientUid { get; set; }
        public string ClientOutUid { get; set; }
        public long Sum { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateEdit { get; set; }
        public int Status { get; set; }
        public int TotalCount { get; set; }

        public int PromosLeft { get; set; }
    }
}
