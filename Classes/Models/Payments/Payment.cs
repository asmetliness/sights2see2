﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GidTaxiApi.Classes.Models.Payments
{
    public class Payment
    {
        public string Uid { get; set; }
        public string ClientUid { get; set; }
        public long Sum { get; set; }
        public int Status { get; set; }
        public string Currency { get; set; }
        public DateTime DateCreate { get; set; }
        public DateTime DateEdit { get; set; }
        public long Discount { get; set; }
        public string PreOrder { get; set; }
        public bool Cashless { get; set; }
    }

    public class PaymentParams
    {
        public long Sum { get; set; }
        public string Currency { get; set; }

        //public bool Cashless { get; set; }
        public string PreOrder { get; set; }
    }
}
