namespace GidTaxiApi.Classes.Models.DataExchange
{
    /// <summary>
    /// Пакет обмена
    /// </summary>
    public class Packet
    {
        public string Uid { get; set; }
        public int Id { get; set; }
        public string Session { get; set; }
        public string Hash { get; set; }
        public Result Result { get; set; }
        public object Data { get; set; }

        public Packet()
        {
            this.Result = new Result();
        }
    }

    /// <summary>
    /// Результат
    /// </summary>
    public class Result
    {
        public string Code { get; set; }
        public string Info { get; set; }
    }
}