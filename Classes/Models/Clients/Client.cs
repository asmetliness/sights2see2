using System;
using System.Collections.Generic;

namespace GidTaxiApi.Classes.Models.Clients
{
    /// <summary>
    /// Клиент системы
    /// </summary>
    public class Client
    {
        public string Uid { get; set; }
        public string EMail { get; set; }
        public string Phone { get; set; }
        public string Session { get; set; }
        public string Pass { get; set; }
        public string FIO { get; set; }
        public bool IsCourier { get; set; }
        public List<ClientParam> ClientParams { get; set; }
        public string ActivationCode { get; set; }

        public Client()
        {
            this.ClientParams = new List<ClientParam>();
        }
    }
    
    /// <summary>
    /// QR-Код для 
    /// </summary>
    public class ClientQR
    {
        public string QRCode { get; set; }
    }

    /// <summary>
    /// Параметр клиента (список возможных параметров в таблице ClientsParamTypes)
    /// </summary>
    public class ClientParam
    {
        public string Ident { get; set; }
        public string ValString { get; set; }
        public DateTime ValDateTime { get; set; } = DateTime.Now;
        public long ValBigInt { get; set; }
    }

    /// <summary>
    /// Для авторизации через пароль логин
    /// </summary>
    public class ClientAutorize
    {
        public string Login { get; set; }
        public string Pass { get; set; }
        public string Code { get; set; }
    }
    
    
    public class LoginPresent
    {
        public bool State { get; set; }
    }
    
    public class NewPass
    {
        public string Phone { get; set; } = "";
        public string OldPass { get; set; } = "";
        public string Pass { get; set; } = "";
    }

}