﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GidTaxiApi.Classes.Models.ClientsPayments
{
    public class ClientPayment
    {
        public string Uid { get; set; }
        public string ClientUid { get; set; }
        public string AccountId { get; set; }
        public string Identification { get; set; }
        public DateTime DateCreate { get; set; }
        public DateTime DateEdit { get; set; }
        public DateTime DateExpiration { get; set; }
        public int Status { get; set; }

        public string CardholderName { get; set; }

        //public string CardCryptogramPacket { get; set; }

        public string CardMask { get; set; }
        public string CardNumber { get; set; }
        public string CardType { get; set; }
    }


    public class ClientPaymentParams
    {
        public string Identification { get; set; }
        public DateTime DateExpiration { get; set; }
        public string AccountId { get; set; }
    }

    public class ClientPaymentMaskParams
    {
        public string CardholderName { get; set; }
        public string Mask { get; set; }
    }
}
