﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GidTaxiApi.Classes.Models.Taxi
{
    public class CreateOrderParams
    {
        public string Currency { get; set; }
        public List<CheckPointAddress> adresses { get; set; } = new List<CheckPointAddress>();
        public int Type { get; set; }
    }
}
