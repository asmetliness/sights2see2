﻿using GidTaxiApi.Classes.Models.CheckPoints;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GidTaxiApi.Classes.Models.Taxi
{
    public class TaxiDistance
    {
        public int Distance { get; set; }
        public int Duration { get; set; }

        public TaxiDistance(int distance, int duration)
        {
            Distance = distance;
            Duration = duration;
        }
        public TaxiDistance()
        {
        }
    }
    public class Address2
    {
        public string City { get; set; }
        public string Point { get; set; }
        public string Street { get; set; }
        public string House { get; set; }
        public string Kind { get; set; }
        public double Lat { get; set; }
        public double Lon { get; set; }
    }

    public class CheckPointAddress
    {
        public string address { get; set; } = "";
        public double lat { get; set; }
        public double lon { get; set; }

        public string uid { get; set; }
    }
    public class Address
    {
        public string address { get; set; }
        public double lat { get; set; }
        public double lon { get; set; }

        public override string ToString()
        {
            return $"(address: {address}, lat: {lat}, lon: {lon})";
        }
    }

    public class FullAddress
    {
        public string address { get; set; }
        public decimal lat { get; set; }
        public decimal lon { get; set; }
        public int zone_id { get; set; }
        public int parking_id { get; set; }
    }

    public class Order
    {
        public List<FullAddress> addresses = new List<FullAddress>();
        public string phone { get; set; }
        public string source_time { get; set; }
        public string comment { get; set; }
        public bool id_prior { get; set; }
        public int tariff_id { get; set; }
        public string phone_to_dial { get; set; }
        public int server_time_offset { get; set; }
        public bool use_cashless { get; set; }
        public bool check_duplicate { get; set; }
        public bool use_bonus { get; set; }
        public int crew_group_id { get; set; }
        public int[] order_params { get; set; }
    }

    public class CheckOrderStateParam
    {
        public string order_id { get; set; }
    }

    public class ConnectClientParam
    {
        public string order_id { get; set; }
    }
    public class Zone
    {
        public int zone_id { get; set; }
    }

    public class AnalyzeRouteRequest
    {
        public bool get_full_route_coords { get; set; }
        public List<Address> addresses { get; set; }
    }

     public class AnalyzeRouteParam
    {
        public int Type { get; set; }
        public List<Address> adresses { get; set; } = new List<Address>();
    }

    public class GetAddressesParam
    {
        public string City { get; set; }
        public string Address { get; set; }
    }
    public class CalculatePriceRequest
    {
        public int tariff_id { get; set; }
        public string source_time { get; set; }
        public int source_zone_id { get; set; }
        public int dest_zone_id { get; set; }
        public double distance_city { get; set; }
        public string phone { get; set; }
        public int crew_group_id { get; set; }

        public List<Zone> stops { get; set; } = new List<Zone>();

        public List<int> order_params = new List<int>();
    }

    public class CancelOrderParam
    {
        public string order_id { get; set; }
    }
    
}
