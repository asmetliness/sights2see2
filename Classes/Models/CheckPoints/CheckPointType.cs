namespace GidTaxiApi.Classes.Models.CheckPoints
{
    /// <summary>
    /// Тип экскурсионного места (фонтан, музей, театр и т.п.)
    /// </summary>
    public class CheckPointType
    {
        public long Id { get; set; }
        public string Name { get; set; }
    }    
}