using System;

namespace GidTaxiApi.Classes.Models.CheckPoints
{
    /// <summary>
    /// Отзыв по экскурсионному месту
    /// </summary>
    public class FeedBack
    {
        public DateTime DateCreate { get; set; }
        public string Header { get; set; }
        public string Comment { get; set; }
        public int Rate { get; set; }
    }
}