using System;
using GidTaxiApi.Classes.Models.Addresses;
using System.Collections.Generic;

namespace GidTaxiApi.Classes.Models.CheckPoints
{
    public class CheckPoint
    {
        public long Id { get; set; }
        public string Uid { get; set; }
        public long Order { get; set; }
        public string Name { get; set; }
        public bool Active { get; set; }
        public Lang Lang { get; set; }
        public Address Address { get; set; }
        public string Description { get; set; }
        public DateTime DateCreate { get; set; }
        public DateTime DateEdit { get; set; }
        public decimal Rating { get; set; }
        public CheckPointType CheckPointType { get; set; }
        public string MainImgFileName { get; set; }
        
        public string Audio { get; set; }

        public double RatingRaius { get; set; }
        
        
        public string WorkDays { get; set; }
        public string WorkHours { get; set; }

        public string HashTags { get; set; }

        public double Lat { get; set; }

        public double Lon { get; set; }
        
        
        public CheckPoint()
        {
            this.Address = new Address();
            this.CheckPointType = new CheckPointType();
            this.Lang = new Lang();
        }

        public CheckPoint(long id, string uid, long order, string name, Lang lang, string description
            , DateTime dateCreate, DateTime dateEdit, decimal rating, CheckPointType checkPointType,
            string mainImgFileName, string audio, string workDays, string workHours, string hashTags, double lat, double lon,
            string gps, string country, string city, string street, string house, string radius)
        {
            Id = id;
            Uid = uid;
            Order = order;
            Name = name;
            Lang = lang;
            Description = description;
            DateCreate = dateCreate;
            DateEdit = dateEdit;
            Rating = rating;
            CheckPointType = checkPointType;
            MainImgFileName = mainImgFileName;
            Audio = audio;
            WorkDays = workDays;
            WorkHours = workHours;
            HashTags = hashTags;
            Lat = lat;
            Lon = lon;
            Address = new Address
            {
                Gps = gps,
                Country = country,
                City = city,
                Street = street,
                House = house,
                Radius = radius
            };
        }
    }

    public class Address
    {
        public string Gps { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
        public string Street { get; set; }
        public string House { get; set; }
        public string Radius { get; set; }
    }
    
    public class LangParam
    {
        public int LangId { get; set; }
    }

    public class Lang
    {
        public long Id { get; set; }
        public string Ident { get; set; }
        public string Name { get; set; }
    }

    public class CheckPointView
    {
        public CheckPoint CheckPoint { get; set; }
        public List<Lang> Langs { get; set; }
        public List<CheckPointType> CheckPointTypes { get; set; }
    }

    public class GetCheckPointsParam
    {
        public long Lang { get; set; }
        public GeoView GeoView { get; set; }
    }
    public class GeoView
    {
        public decimal GPS1 { get; set; }
        public decimal GPS2 { get; set; }
    }

    public class GetCheckPointsByTwoPointsParam
    {
        public long Lang { get; set; }
        public GeoPath GeoPath { get; set; }
    }

    public class GeoPath
    {
        public GeoView GPSPoint1 { get; set; }
        public GeoView GPSPoint2 { get; set; }
    }

    public class GetCheckPointParam
    {
        public string CheckPointUid { get; set; }
        public long Lang { get; set; }
    }

}