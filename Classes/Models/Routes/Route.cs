using System.Collections.Generic;
using GidTaxiApi.Classes.Models.CheckPoints;

namespace GidTaxiApi.Classes.Models.Routes
{
    /// <summary>
    /// Экскурсия - Маршрут
    /// </summary>
    public class Route
    {
        public long Id { get; set; }
        public string Uid { get; set; }
        public string Name { get; set; }
        public long Order { get; set; }
        public List<CheckPoint> CheckPoints { get; set; }
        public decimal Rating { get; set; }
        public string MainImgFileName { get; set; }
        public string Audio { get; set; }
        public string Description { get; set; }
        public string HashTags { get; set; }
    }


    public class GetCheckPointByRouteParam
    {
        public long Id { get; set; }
        public long Lang { get; set; }
    }

    public class CheckPointParam
    {
        public int Lang { get; set; }
        public long CheckPointId { get; set; }
    }
}