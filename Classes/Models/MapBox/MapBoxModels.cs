﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GidTaxiApi.Classes.Models.MapBox
{

    public class Mapbox
    {
        public Mapbox()
        {
        }

        [JsonProperty(PropertyName = "code")]
        public string Code { get; set; }
        [JsonProperty(PropertyName = "uuid")]
        public string Uuid { get; set; }

        [JsonProperty(PropertyName = "routes")]
        public MapboxRoute[] Routes { get; set; }
        [JsonProperty(PropertyName = "waypoints")]
        public MapboxWaypoint[] Waypoints { get; set; }
    }

    public class MapboxWaypoint
    {
        public MapboxWaypoint()
        {
        }

        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }
        [JsonProperty(PropertyName = "location")]
        public double[] Location { get; set; }
        [JsonProperty(PropertyName = "distance")]
        public double Distance { get; set; }
    }

    public class MapboxRoute
    {
        public MapboxRoute()
        {
        }

        [JsonProperty(PropertyName = "geometry")]
        public string Geometry { get; set; }
        [JsonProperty(PropertyName = "duration")]
        public double Duration { get; set; }
        [JsonProperty(PropertyName = "distance")]
        public double Distance { get; set; }
        [JsonProperty(PropertyName = "weight_name")]
        public string WeightName { get; set; }
        [JsonProperty(PropertyName = "weight")]
        public double Weight { get; set; }
        [JsonProperty(PropertyName = "voiceLocale")]
        public string VoiceLocale { get; set; }

        [JsonProperty(PropertyName = "legs")]
        public MapboxRouteLeg[] RouteLegs { get; set; }
    }

    public class MapboxRouteLeg
    {
        public MapboxRouteLeg()
        {
        }

        [JsonProperty(PropertyName = "summary")]
        public string Summary { get; set; }
        [JsonProperty(PropertyName = "weight")]
        public double Weight { get; set; }
        [JsonProperty(PropertyName = "duration")]
        public double Duration { get; set; }
        [JsonProperty(PropertyName = "distance")]
        public double Distance { get; set; }

        [JsonProperty(PropertyName = "annotation")]
        public MapboxAnnotation Annotation { get; set; }

        [JsonProperty(PropertyName = "steps")]
        public MapboxRouteStep[] RouteSteps { get; set; }
    }


    public class MapboxAnnotation
    {
        public MapboxAnnotation()
        {
        }

        [JsonProperty(PropertyName = "duration")]
        public double[] Duration { get; set; }
        [JsonProperty(PropertyName = "distance")]
        public double[] Distance { get; set; }
        [JsonProperty(PropertyName = "speed")]
        public double[] Speed { get; set; }
        [JsonProperty(PropertyName = "congestion")]
        public string Congestion { get; set; }
    }

    public class MapboxRouteStep
    {
        public MapboxRouteStep()
        {
        }

        [JsonProperty(PropertyName = "weight")]
        public double Weight { get; set; }
        [JsonProperty(PropertyName = "duration")]
        public double Duration { get; set; }
        [JsonProperty(PropertyName = "distance")]
        public double Distance { get; set; }
        [JsonProperty(PropertyName = "driving_side")]
        public string DrivingSide { get; set; }
        [JsonProperty(PropertyName = "geometry")]
        public string Geometry { get; set; }
        [JsonProperty(PropertyName = "mode")]
        public string Mode { get; set; }
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }
        [JsonProperty(PropertyName = "destinations")]
        public string Destinations { get; set; }
        [JsonProperty(PropertyName = "exits")]
        public string Exits { get; set; }

        [JsonProperty(PropertyName = "maneuver")]
        public MapboxStepManeuver StepManeuver { get; set; }

        [JsonProperty(PropertyName = "intersections")]
        public MapboxIntersection[] Intersections { get; set; }
    }

    public class MapboxIntersection
    {
        public MapboxIntersection()
        {
        }

        [JsonProperty(PropertyName = "out")]
        public int Out { get; set; }
        [JsonProperty(PropertyName = "in")]
        public int In { get; set; }
        [JsonProperty(PropertyName = "entry")]
        public bool[] Entry { get; set; }
        [JsonProperty(PropertyName = "bearings")]
        public int[] Bearings { get; set; }
        [JsonProperty(PropertyName = "location")]
        public double[] Location { get; set; }
        [JsonProperty(PropertyName = "classes")]
        public string[] Classes { get; set; }

        [JsonProperty(PropertyName = "lanes")]
        public MapboxLane[] Lanes { get; set; }
    }

    public class MapboxStepManeuver
    {
        public MapboxStepManeuver()
        {
        }

        [JsonProperty(PropertyName = "bearing_after")]
        public int BearingAfter { get; set; }
        [JsonProperty(PropertyName = "bearing_before")]
        public int BearingBefore { get; set; }
        [JsonProperty(PropertyName = "modifier")]
        public string Modifier { get; set; }
        [JsonProperty(PropertyName = "type")]
        public string Type { get; set; }
        [JsonProperty(PropertyName = "instruction")]
        public string Instruction { get; set; }

        [JsonProperty(PropertyName = "location")]
        public double[] Location { get; set; }
    }

    public class MapboxLane
    {
        public MapboxLane()
        {
        }

        [JsonProperty(PropertyName = "valid")]
        public bool Valid { get; set; }
        [JsonProperty(PropertyName = "indications")]
        public string[] Indications { get; set; }
    }

    public class MapboxGeo
    {
        public MapboxGeo()
        {
        }

        [JsonProperty(PropertyName = "type")]
        public string Type { get; set; }
        [JsonProperty(PropertyName = "attribution")]
        public string Attribution { get; set; }
        [JsonProperty(PropertyName = "query")]
        public double[] Query { get; set; }
        [JsonProperty(PropertyName = "features")]
        public MapboxFeature[] Features { get; set; }
    }

    public class MapboxFeature
    {
        public MapboxFeature()
        {
        }

        [JsonProperty(PropertyName = "id")]
        public string ID { get; set; }
        [JsonProperty(PropertyName = "type")]
        public string Type { get; set; }
        [JsonProperty(PropertyName = "place_type")]
        public string[] PlaceType { get; set; }
        [JsonProperty(PropertyName = "relevance")]
        public int Relevance { get; set; }
        [JsonProperty(PropertyName = "text")]
        public string Text { get; set; }
        [JsonProperty(PropertyName = "address")]
        public string Address { get; set; }
        [JsonProperty(PropertyName = "place_name")]
        public string PlaceName { get; set; }
        [JsonProperty(PropertyName = "center")]
        public double[] Center { get; set; }
        [JsonProperty(PropertyName = "matching_text")]
        public string MatchingText { get; set; }
        [JsonProperty(PropertyName = "matching_place_name")]
        public string MatchingPlaceName { get; set; }
        [JsonProperty(PropertyName = "language")]
        public string Language { get; set; }

        [JsonProperty(PropertyName = "properties")]
        public FeatureProperty Properties { get; set; }
        [JsonProperty(PropertyName = "geometry")]
        public FeatureGeometry Geometry { get; set; }
        [JsonProperty(PropertyName = "context")]
        public FeatureContext[] Context { get; set; }
        [JsonProperty(PropertyName = "bbox")]
        public double[] Bbox { get; set; }
    }

    public class FeatureProperty
    {
        public FeatureProperty()
        {
        }

        [JsonProperty(PropertyName = "landmark")]
        public bool Landmark { get; set; }
        [JsonProperty(PropertyName = "category")]
        public string Category { get; set; }
        [JsonProperty(PropertyName = "accuracy")]
        public string Accuracy { get; set; }
        [JsonProperty(PropertyName = "address")]
        public string Address { get; set; }
        [JsonProperty(PropertyName = "maki")]
        public string Maki { get; set; }
        [JsonProperty(PropertyName = "wikidata")]
        public string Wikidata { get; set; }
        [JsonProperty(PropertyName = "short_code")]
        public string ShortCode { get; set; }
    }

    public class FeatureGeometry
    {
        public FeatureGeometry()
        {
        }
        [JsonProperty(PropertyName = "coordinates")]
        public double[] Coordinates { get; set; }
        [JsonProperty(PropertyName = "type")]
        public string Type { get; set; }
    }

    public class FeatureContext
    {
        public FeatureContext()
        {
        }

        [JsonProperty(PropertyName = "id")]
        public string ID { get; set; }
        [JsonProperty(PropertyName = "text")]
        public string Text { get; set; }
        [JsonProperty(PropertyName = "wikidata")]
        public string Wikidata { get; set; }
        [JsonProperty(PropertyName = "short_code")]
        public string ShortCode { get; set; }
    }

    public class Point
    {
        public Point(double lon, double lat)
        {
            Latitude = lat;
            Longitude = lon;
            IsCheckPoint = false;
        }
        public Point(double lon, double lat, bool check)
        {
            Latitude = lat;
            Longitude = lon;
            IsCheckPoint = check;
        }
        public bool IsCheckPoint { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public string LatitudeStr { get { return Latitude.ToString().Replace(",", "."); } }
        public string LongitudeStr { get { return Longitude.ToString().Replace(",", "."); } }
    }

}
