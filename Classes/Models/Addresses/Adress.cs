namespace GidTaxiApi.Classes.Models.Addresses
{
            /// <summary>
            /// Адрес
            /// </summary>
            public class Address
            {
                public long Id { get; set; }
                public string Country { get; set; }
                public string City { get; set; }
                public string Street { get; set; }
                public string House { get; set; }
            }
}