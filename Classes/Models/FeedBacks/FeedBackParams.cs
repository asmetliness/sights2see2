﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GidTaxiApi.Classes.Models.FeedBacks
{
    public class FeedBackParams
    {
        public string Comment { get; set; }
        public int Rate { get; set; }
        public long Lang { get; set; }

        public int Type { get; set; }

        public string DateTime { get; set; }
        public string Phone { get; set; }

        public string CheckPointUid { get; set; }
        public string RouteUid { get; set; }
    }

    public class FeedBackEditParams
    {
        public string Comment { get; set; }
        public int Rate { get; set; }
        public string FeedBackUid { get; set; }
    }

    public class FeedBackGetParams
    {
        public string Uid { get; set; }
        public long Lang { get; set; }
    }

}
