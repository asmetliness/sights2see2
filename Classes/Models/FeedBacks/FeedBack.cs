﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GidTaxiApi.Classes.Models.FeedBacks
{
    public class FeedBack
    {
        public string Uid { get; set; }
        public string ClientUid { get; set; }
        public string RouteUid { get; set; } 
        public string CheckPointUid { get; set; } 
        public string Comment { get; set; } 
        public int Rate { get; set; }
        public int Type { get; set; }
        public DateTime DateCreate { get; set; }
        public long Lang { get; set; }
    }

    public class ClientsFeedBack
    {
        public int Rate { get; set; }
        public string Comment { get; set; }
        public DateTime DateCreate { get; set; }
        public string FIO { get; set; }
        public string Photo { get; set; }
        public string ClientUid { get; set; }
    }

}
