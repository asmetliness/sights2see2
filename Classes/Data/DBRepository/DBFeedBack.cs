﻿using GidTaxiApi.Classes.Models.DataExchange;
using GidTaxiApi.Classes.Models.FeedBacks;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;

namespace GidTaxiApi.Classes.Data.DBRepository
{
    public class DBFeedBack
    {
        /// <summary>
        /// Добавление отзыва клиента
        /// </summary>
        /// <param name="param"></param>
        /// <param name="session"></param>
        /// <returns></returns>
        public static Packet AddFeedBack(FeedBackParams param, string session)
        {
            Packet result = new Packet();
            FeedBack feedBack = new FeedBack();
            try
            {
                using (SqlConnection con = new SqlConnection(DbInMemory.Params.ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("[dbo].[MOB_FeedBack_AddNew]", con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;

                        SqlParameter p_rate = new SqlParameter
                        {
                            ParameterName = "@p_rate",
                            SqlDbType = SqlDbType.Int,
                            Direction = ParameterDirection.Input,
                            Value = param.Rate
                        };
                        cmd.Parameters.Add(p_rate);

                        SqlParameter p_comment = new SqlParameter
                        {
                            ParameterName = "@p_comment",
                            SqlDbType = SqlDbType.VarChar,
                            Direction = ParameterDirection.Input,
                            Value = param.Comment
                        };
                        cmd.Parameters.Add(p_comment);


                        SqlParameter p_lang = new SqlParameter
                        {
                            ParameterName = "@p_lang",
                            SqlDbType = SqlDbType.BigInt,
                            Direction = ParameterDirection.Input,
                            Value = param.Lang
                        };
                        cmd.Parameters.Add(p_lang);

                        SqlParameter p_type = new SqlParameter
                        {
                            ParameterName = "@p_type",
                            SqlDbType = SqlDbType.Int,
                            Direction = ParameterDirection.Input,
                            Value = param.Type
                        };
                        cmd.Parameters.Add(p_type);


                        SqlParameter p_route = new SqlParameter
                        {
                            ParameterName = "@p_route",
                            SqlDbType = SqlDbType.VarChar,
                            Direction = ParameterDirection.Input,
                            Value = param.RouteUid
                        };
                        cmd.Parameters.Add(p_route);

                        SqlParameter p_checkPoint = new SqlParameter
                        {
                            ParameterName = "@p_checkPoint",
                            SqlDbType = SqlDbType.VarChar,
                            Direction = ParameterDirection.Input,
                            Value = param.CheckPointUid
                        };
                        cmd.Parameters.Add(p_checkPoint);

                        DateTime date;
                        bool converted = DateTime.TryParse(param.DateTime, new CultureInfo("de-DE"), DateTimeStyles.None, out date);
                        SqlParameter p_date = new SqlParameter
                        {
                            ParameterName = "@p_date",
                            SqlDbType = SqlDbType.DateTime,
                            Direction = ParameterDirection.Input,
                            Value = converted ? date : DateTime.Now
                        };
                        cmd.Parameters.Add(p_date);

                        SqlParameter p_phone = new SqlParameter
                        {
                            ParameterName = "@p_phone",
                            SqlDbType = SqlDbType.VarChar,
                            Direction = ParameterDirection.Input,
                            Value = param.Phone
                        };
                        cmd.Parameters.Add(p_phone);

                        SqlParameter p_session = new SqlParameter
                        {
                            ParameterName = "@p_session",
                            SqlDbType = SqlDbType.VarChar,
                            Direction = ParameterDirection.Input,
                            Value = session
                        };
                        cmd.Parameters.Add(p_session);

                        con.Open();
                        SqlDataReader reader = cmd.ExecuteReader();
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                result.Result.Code = reader["Code"].ToString();
                                result.Result.Info = reader["Info"].ToString();
                            }

                            if (reader.NextResult())
                            {

                                while (reader.Read())
                                {
                                    FeedBack fb = new FeedBack
                                    {
                                        Uid = reader["Uid"].ToString(),
                                        ClientUid = reader.IsDBNull(reader.GetOrdinal("ClientUid")) ? null : reader["ClientUid"].ToString(),
                                        Comment = reader.GetString(reader.GetOrdinal("Comment")),
                                        Rate = reader.GetInt32(reader.GetOrdinal("Rate")),
                                        Lang = reader.IsDBNull(reader.GetOrdinal("LangId")) ? 2 : reader.GetInt64(reader.GetOrdinal("LangId")),
                                        DateCreate = reader.GetDateTime(reader.GetOrdinal("DateCreate")),
                                        Type = reader.GetInt32(reader.GetOrdinal("Type")),
                                        CheckPointUid = param.CheckPointUid,
                                        RouteUid = param.RouteUid,
                                    };
                                    feedBack = fb;
                                }

                            }
                        }
                        else
                        {
                            con.Close();
                        }
                        reader.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                Program.Logger.Error(ex, ex.Message);
            }
            result.Data = feedBack;
            return result;
        }

        public static Packet EditFeedBack(FeedBackEditParams param, string session)
        {
            Packet result = new Packet();

            FeedBack feedBack = new FeedBack();
            try
            {
                using (SqlConnection con = new SqlConnection(DbInMemory.Params.ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("[dbo].[MOB_FeedBack_Edit]", con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;

                        SqlParameter p_feedBackUid = new SqlParameter
                        {
                            ParameterName = "@p_feedBackUid",
                            SqlDbType = SqlDbType.VarChar,
                            Direction = ParameterDirection.Input,
                            Value = param.FeedBackUid
                        };
                        cmd.Parameters.Add(p_feedBackUid);

                        SqlParameter p_comment = new SqlParameter
                        {
                            ParameterName = "@p_comment",
                            SqlDbType = SqlDbType.VarChar,
                            Direction = ParameterDirection.Input,
                            Value = param.Comment
                        };
                        cmd.Parameters.Add(p_comment);

                        SqlParameter p_rate = new SqlParameter
                        {
                            ParameterName = "@p_rate",
                            SqlDbType = SqlDbType.Int,
                            Direction = ParameterDirection.Input,
                            Value = param.Rate
                        };
                        cmd.Parameters.Add(p_rate);

                        SqlParameter p_session = new SqlParameter
                        {
                            ParameterName = "@p_session",
                            SqlDbType = SqlDbType.VarChar,
                            Direction = ParameterDirection.Input,
                            Value = session
                        };
                        cmd.Parameters.Add(p_session);

                        con.Open();
                        SqlDataReader reader = cmd.ExecuteReader();
                        if(reader.HasRows)
                        {
                            while(reader.Read())
                            {
                                result.Result.Code = reader["Code"].ToString();
                                result.Result.Info = reader["Info"].ToString();
                            }
                            if(reader.NextResult())
                            {
                                while(reader.Read())
                                {
                                    FeedBack fb = new FeedBack
                                    {
                                        Uid = reader["Uid"].ToString(),
                                        ClientUid = reader.IsDBNull(reader.GetOrdinal("ClientUid")) ? null : reader["ClientUid"].ToString(),
                                        Comment = reader.GetString(reader.GetOrdinal("Comment")),
                                        Rate = reader.GetInt32(reader.GetOrdinal("Rate")),
                                        Lang = reader.IsDBNull(reader.GetOrdinal("LangId")) ? 2 : reader.GetInt64(reader.GetOrdinal("LangId")),
                                        DateCreate = reader.GetDateTime(reader.GetOrdinal("DateCreate")),
                                        Type = reader.GetInt32(reader.GetOrdinal("Type")),
                                        CheckPointUid = reader.IsDBNull(reader.GetOrdinal("CheckPointUid")) ? null : reader["CheckPointUid"].ToString(),
                                        RouteUid = reader.IsDBNull(reader.GetOrdinal("RouteUid")) ? null : reader["RouteUid"].ToString(),
                                    };
                                    feedBack = fb;
                                }
                            }
                        }
                        else
                        {
                            con.Close();
                        }
                        reader.Close();
                    }
                }
            }
            catch(Exception ex)
            {
                Program.Logger.Error(ex, ex.Message);
            }
            result.Data = feedBack;
            return result;
        }

        public static Packet GetAllFeedBack(FeedBackGetParams param)
        {
            Packet result = new Packet();
            List<ClientsFeedBack> feedBacks = new List<ClientsFeedBack>();

            try
            {
                using (SqlConnection con = new SqlConnection(DbInMemory.Params.ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("[dbo].[MOB_FeedBack_Get]", con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;

                        SqlParameter p_uid = new SqlParameter
                        {
                            ParameterName = "@p_uid",
                            SqlDbType = SqlDbType.VarChar,
                            Direction = ParameterDirection.Input,
                            Value = param.Uid
                        };
                        cmd.Parameters.Add(p_uid);

                        SqlParameter p_lang = new SqlParameter
                        {
                            ParameterName = "@p_lang",
                            SqlDbType = SqlDbType.BigInt,
                            Direction = ParameterDirection.Input,
                            Value = param.Lang
                        };
                        cmd.Parameters.Add(p_lang);

                        con.Open();
                        SqlDataReader reader = cmd.ExecuteReader();
                        if(reader.HasRows)
                        {
                            while(reader.Read())
                            {
                                result.Result.Code = reader["Code"].ToString();
                                result.Result.Info = reader["Info"].ToString();
                            }
                            if(reader.NextResult())
                            {
                                while(reader.Read())
                                {
                                    ClientsFeedBack fb = new ClientsFeedBack
                                    {
                                        Rate = reader.GetInt32(reader.GetOrdinal("Rate")),
                                        Comment = reader["Comment"].ToString(),
                                        DateCreate = reader.GetDateTime(reader.GetOrdinal("DateCreate")),
                                        FIO = reader["FIO"].ToString(),
                                        Photo = reader["Photo"].ToString(),
                                        ClientUid = reader["ClientUid"].ToString()
                                    };
                                    feedBacks.Add(fb);
                                }
                            }
                        }
                        else
                        {
                            con.Close();
                        }
                        reader.Close();                      
                    }
                }
            }
            catch(Exception ex)
            {
                Program.Logger.Error(ex, ex.Message);
            }
            result.Data = feedBacks;
            return result;
        }
    }
}
