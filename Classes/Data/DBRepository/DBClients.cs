using System.Data;
using System.Data.SqlClient;
using GidTaxiApi.Classes.Models.DataExchange;
using GidTaxiApi.Classes.Models.Clients;
using System.Text.RegularExpressions;
using System;
using System.Net;
using GidTaxiApi.Classes.Models.Langs;
using System.Collections.Generic;

namespace GidTaxiApi.Classes.Data.DBRepository
{
    public class DBClients
    {
        public static Packet GetSession(string phone)
        {
            Packet result1 = new Packet();
            string result = "";
            try
            {
                using (SqlConnection con = new SqlConnection(DbInMemory.Params.ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("[dbo].[GetSession]", con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.Parameters.Add(new SqlParameter
                        {
                            ParameterName = "@phone",
                            Direction = ParameterDirection.Input,
                            SqlDbType = SqlDbType.VarChar,
                            Value = phone
                        });

                        con.Open();
                        SqlDataReader reader = cmd.ExecuteReader();
                        if(reader.HasRows)
                        {
                            while(reader.Read())
                            {
                                result = reader["Session"].ToString();
                            }
                        }
                        else
                        {
                            con.Close();
                        }
                        reader.Close();
                    }
                }
            } catch(Exception ex)
            {
                Program.Logger.Error(ex, ex.Message);
            }
            result1.Data = result;
            return result1;
        }

        /// <summary>
        /// Возвращает только числа из строки
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        private static string ReturnJustNumbers(string text)
        {
            string shabl = "[0-9]+";   
            MatchCollection match = Regex.Matches(text, shabl); 
            string sum = "";
            foreach (Match m in match)
            { 
                sum += m.Value; 
            }
            return sum;
        }
        
        /// <summary>
        /// Установка нового пароля пользователя
        /// </summary>
        /// <param name="client_in"></param>
        /// <returns></returns>
        public static Packet NewPass(NewPass client_in)
        {
            Packet packet = new Packet();
            packet.Result.Code = "0";
            packet.Result.Info = "OK";
            client_in.Phone = ReturnJustNumbers(client_in.Phone);

            try
            {
                using (SqlConnection con = new SqlConnection(DbInMemory.Params.ConnectionString))
                {
                    // Авторизуем клиента
                    using (SqlCommand cmd = new SqlCommand("[dbo].[MOB_ClientSendPassword]", con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;

                        SqlParameter p_phone = new SqlParameter
                        {
                            ParameterName = "@phone",
                            SqlDbType = SqlDbType.VarChar,
                            Direction = ParameterDirection.Input,
                            Value = client_in.Phone
                        };
                        cmd.Parameters.Add(p_phone);

                        SqlParameter p_pass = new SqlParameter
                        {
                            ParameterName = "@password",
                            SqlDbType = SqlDbType.VarChar,
                            Direction = ParameterDirection.Input,
                            Value = DbInMemory.CreateMD5(client_in.Pass + DbInMemory.Params.ServiceKey)
                        };
                        cmd.Parameters.Add(p_pass);

                        con.Open();
                        SqlDataReader reader = cmd.ExecuteReader();
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {

                                packet.Result.Code = reader.GetInt32(reader.GetOrdinal("Result")).ToString();
                                packet.Result.Info = reader["Info"].ToString();

                            }

                        }
                        else
                        {
                            reader.Close();
                            con.Close();
                            packet.Result.Code = "K003";
                            packet.Result.Info = "Данные из БД не получены";
                        }
                        reader.Close();
                    }
                }

            }
            catch (Exception ex)
            {
                packet.Result.Code = "K002";
                packet.Result.Info = "Ошибка при обращении к БД";
                Program.Logger.Error(ex, ex.Message);
            }

            if (packet.Result.Code == "0")
            {
                // Отправляем СМС сообщение
                try
                {
                    // Пока, так топорно, после надо вынести в таск с занесением в лог БД
                    WebRequest request_sms = WebRequest.Create("https://sms.ru/sms/send?api_id=02BAF581-2DF8-77E3-6C85-4B7CF9ECE4BD&to=" + client_in.Phone + "&msg=GidTaxi Ваш новый пароль " + client_in.Pass + "&json=1");
                    WebResponse response_sms = request_sms.GetResponse();

                    response_sms.Close();
                }
                catch (Exception ex)
                {
                    Program.Logger.Error(ex.Message);
                }
            }

            return packet;
        }

        
                /// <summary>
        /// Изменение пароля пользователем
        /// </summary>
        /// <param name="client_in"></param>
        /// <returns></returns>
        public static Packet ChangePass(NewPass client_in, string session)
        {
            Packet packet = new Packet();
            packet.Result.Code = "0";
            packet.Result.Info = "OK";
            client_in.Phone = ReturnJustNumbers(client_in.Pass);

            try
            {
                using (SqlConnection con = new SqlConnection(DbInMemory.Params.ConnectionString))
                {
                    // Авторизуем клиента
                    using (SqlCommand cmd = new SqlCommand("[dbo].[MOB_ClientSetNewPassword]", con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;

                        SqlParameter p_session = new SqlParameter
                        {
                            ParameterName = "@session",
                            SqlDbType = SqlDbType.VarChar,
                            Direction = ParameterDirection.Input,
                            Value = session
                        };
                        cmd.Parameters.Add(p_session);

                        SqlParameter p_pass = new SqlParameter
                        {
                            ParameterName = "@password",
                            SqlDbType = SqlDbType.VarChar,
                            Direction = ParameterDirection.Input,
                            Value = client_in.Pass
                        };
                        cmd.Parameters.Add(p_pass);

                        SqlParameter p_old_pass = new SqlParameter
                        {
                            ParameterName = "@old_password",
                            SqlDbType = SqlDbType.VarChar,
                            Direction = ParameterDirection.Input,
                            Value = client_in.OldPass
                        };
                        cmd.Parameters.Add(p_old_pass);

                        con.Open();
                        SqlDataReader reader = cmd.ExecuteReader();
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {

                                packet.Result.Code = reader.GetInt32(reader.GetOrdinal("Result")).ToString();
                                packet.Result.Info = reader["Info"].ToString();

                            }
                        }
                        else
                        {
                            reader.Close();
                            con.Close();
                            packet.Result.Code = "K003";
                            packet.Result.Info = "Данные из БД не получены";
                        }
                        reader.Close();
                    }
                }

            }
            catch (Exception ex)
            {
                packet.Result.Code = "K002";
                packet.Result.Info = "Ошибка при обращении к БД";
                Program.Logger.Error(ex, ex.Message);
            }

            return packet;
        }

        /// <summary>
        /// Проверка логина на существование
        /// </summary>
        /// <param name="client_in"></param>
        /// <returns></returns>
        public static Packet CheckClient(ClientAutorize client_in)
        {
            Packet packet = new Packet();
            packet.Result.Code = "0";
            packet.Result.Info = "OK";

            LoginPresent lp = new LoginPresent(); // Представлен ли логин в системе (проверка)
            
            try
            {
                using (SqlConnection con = new SqlConnection(DbInMemory.Params.ConnectionString))
                {
                    // Авторизуем клиента
                    using (SqlCommand cmd = new SqlCommand("[dbo].[MOB_ClientCheck]", con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;

                        SqlParameter p_login = new SqlParameter
                        {
                            ParameterName = "@login",
                            SqlDbType = SqlDbType.VarChar,
                            Direction = ParameterDirection.Input,
                            Value = client_in.Login
                        };
                        cmd.Parameters.Add(p_login);

                        con.Open();
                        SqlDataReader reader = cmd.ExecuteReader();
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {

                                packet.Result.Code = reader.GetInt32(reader.GetOrdinal("Result")).ToString();
                                packet.Result.Info = reader["Info"].ToString();

                            }
                            if (reader.NextResult())
                            {
                                while (reader.Read())
                                {
                                    lp.State = reader.GetBoolean(reader.GetOrdinal("Active"));
                                }
                            }
                        }
                        else
                        {
                            reader.Close();
                            con.Close();
                            packet.Result.Code = "K003";
                            packet.Result.Info = "Данные из БД не получены";
                        }
                        reader.Close();
                    }
                }

            }
            catch (Exception ex)
            {
                packet.Result.Code = "K002";
                packet.Result.Info = "Ошибка при обращении к БД";
                Program.Logger.Error(ex, ex.Message);
            }
            packet.Data = lp;
            return packet;
        }

        /// <summary>
        /// Активация клиента
        /// </summary>
        /// <param name="client_in"></param>
        /// <returns></returns>
        public static Packet ActivateClient(ClientAutorize client_in)
        {
            Packet packet = new Packet();
            packet.Result.Code = "0";
            packet.Result.Info = "OK";
            
            Client client = new Client();

            try
            {
                using (SqlConnection con = new SqlConnection(DbInMemory.Params.ConnectionString))
                {
                    // Авторизуем клиента
                    using (SqlCommand cmd = new SqlCommand("[dbo].[MOB_ClientActivate]", con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;

                        SqlParameter p_login = new SqlParameter
                        {
                            ParameterName = "@login",
                            SqlDbType = SqlDbType.VarChar,
                            Direction = ParameterDirection.Input,
                            Value = client_in.Login
                        };
                        cmd.Parameters.Add(p_login);

                        SqlParameter p_pass = new SqlParameter
                        {
                            ParameterName = "@Pass",
                            SqlDbType = SqlDbType.VarChar,
                            Direction = ParameterDirection.Input,
                            Value = client_in.Pass
                        };
                        cmd.Parameters.Add(p_pass);

                        SqlParameter p_code = new SqlParameter
                        {
                            ParameterName = "@code",
                            SqlDbType = SqlDbType.VarChar,
                            Direction = ParameterDirection.Input,
                            Value = client_in.Code
                        };
                        cmd.Parameters.Add(p_code);

                        con.Open();
                        SqlDataReader reader = cmd.ExecuteReader();
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {

                                packet.Result.Code = reader.GetInt32(reader.GetOrdinal("Result")).ToString();
                                packet.Result.Info = reader["Info"].ToString();

                            }
                            if (reader.NextResult())
                            {
                                while (reader.Read())
                                {
                                    client.Uid = reader["Uid"].ToString();
                                    client.FIO = reader["FIO"].ToString();
                                    client.Phone = reader["Phone"].ToString();
                                    client.EMail = reader["EMail"].ToString();
                                    client.IsCourier = reader.GetBoolean(reader.GetOrdinal("IsCourier"));
                                    client.Session = packet.Session = reader["SessionUid"].ToString();
                                }
                            }
                        }
                        else
                        {
                            reader.Close();
                            con.Close();
                            packet.Result.Code = "K003";
                            packet.Result.Info = "Данные из БД не получены";
                        }
                        reader.Close();
                    }
                }

            }
            catch (Exception ex)
            {
                packet.Result.Code = "K002";
                packet.Result.Info = "Ошибка при обращении к БД";
                Program.Logger.Error(ex, ex.Message);
            }
            packet.Data = client;
            return packet;
        }

        /// <summary>
        /// Авторизация клиента
        /// </summary>
        /// <param name="client_in"></param>
        /// <returns></returns>
        public static Packet AutorizeClient(ClientAutorize client_in)
        {
            Packet packet = new Packet();
            packet.Result.Code = "0";
            packet.Result.Info = "OK";
            
            Client client = new Client();

            try
            {
                using (SqlConnection con = new SqlConnection(DbInMemory.Params.ConnectionString))
                {
                    // Авторизуем клиента
                    using (SqlCommand cmd = new SqlCommand("[dbo].[MOB_ClientAutorize]", con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;

                        SqlParameter p_login = new SqlParameter
                        {
                            ParameterName = "@login",
                            SqlDbType = SqlDbType.VarChar,
                            Direction = ParameterDirection.Input,
                            Value = client_in.Login
                        };
                        cmd.Parameters.Add(p_login);

                        SqlParameter p_pass = new SqlParameter
                        {
                            ParameterName = "@Pass",
                            SqlDbType = SqlDbType.VarChar,
                            Direction = ParameterDirection.Input,
                            Value = client_in.Pass
                        };
                        cmd.Parameters.Add(p_pass);

                        con.Open();
                        SqlDataReader reader = cmd.ExecuteReader();
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {

                                    packet.Result.Code = reader.GetInt32(reader.GetOrdinal("Result")).ToString();
                                    packet.Result.Info = reader["Info"].ToString();

                            }
                            if (reader.NextResult())
                            {
                                while (reader.Read())
                                {
                                    client.Uid = reader["Uid"].ToString();
                                    client.FIO = reader["FIO"].ToString();
                                    client.Phone = reader["Phone"].ToString();
                                    client.EMail = reader["EMail"].ToString();
                                    
                                    client.Session = packet.Session = reader["SessionUid"].ToString();
                                }
                            }
                            if (reader.NextResult())
                            {
                                while (reader.Read())
                                {
                                    ClientParam p = new ClientParam
                                    {
                                        Ident = reader["Ident"].ToString(),
                                        ValBigInt = reader.IsDBNull(reader.GetOrdinal("ValB")) ? -1 : reader.GetInt64(reader.GetOrdinal("ValB")),
                                        ValDateTime = reader.IsDBNull(reader.GetOrdinal("ValD")) ? DateTime.MinValue : reader.GetDateTime(reader.GetOrdinal("ValD")),
                                        ValString = reader.IsDBNull(reader.GetOrdinal("ValV")) ? "" : reader["ValV"].ToString()
                                    };
                                    client.ClientParams.Add(p);
                                }
                            }
                        }
                        else
                        {
                            reader.Close();
                            con.Close();
                            packet.Result.Code = "K003";
                            packet.Result.Info = "Данные из БД не получены";
                        }
                        reader.Close();
                    }
                }

            }
            catch (Exception ex)
            {
                packet.Result.Code = "K002";
                packet.Result.Info = "Ошибка при обращении к БД";
                Program.Logger.Error(ex, ex.Message);
            }
            packet.Data = client;
            return packet;
        }
                
        /// <summary>
        /// Регистрация клиента
        /// </summary>
        /// <param name="client_in"></param>
        /// <returns></returns>
        public static Packet RegisterClient(Client client_in)
        {
            Packet packet = new Packet();
            packet.Result.Code = "0";
            packet.Result.Info = "OK";

            client_in.Phone = ReturnJustNumbers(client_in.Phone);

            Client client = new Client();

            try
            {
                using (SqlConnection con = new SqlConnection(DbInMemory.Params.ConnectionString))
                {
                    // Авторизуем клиента
                    using (SqlCommand cmd = new SqlCommand("[dbo].[MOB_ClientAdd]", con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;

                        SqlParameter p_f = new SqlParameter
                        {
                            ParameterName = "@F",
                            SqlDbType = SqlDbType.VarChar,
                            Direction = ParameterDirection.Input,
                            Value = client_in.ClientParams.Find(s => s.Ident == "family").ValString ?? ""
                        };
                        cmd.Parameters.Add(p_f);

                        SqlParameter p_i = new SqlParameter
                        {
                            ParameterName = "@I",
                            SqlDbType = SqlDbType.VarChar,
                            Direction = ParameterDirection.Input,
                            Value = client_in.ClientParams.Find(s => s.Ident == "name").ValString ?? ""
                        };
                        cmd.Parameters.Add(p_i);

                        SqlParameter p_o = new SqlParameter
                        {
                            ParameterName = "@O",
                            SqlDbType = SqlDbType.VarChar,
                            Direction = ParameterDirection.Input,
                            Value = client_in.ClientParams.Find(s => s.Ident == "sname").ValString ?? ""
                        };
                        cmd.Parameters.Add(p_o);

                        SqlParameter p_email = new SqlParameter
                        {
                            ParameterName = "@EMail",
                            SqlDbType = SqlDbType.VarChar,
                            Direction = ParameterDirection.Input,
                            Value = client_in.EMail
                        };
                        cmd.Parameters.Add(p_email);

                        SqlParameter p_phone = new SqlParameter
                        {
                            ParameterName = "@Phone",
                            SqlDbType = SqlDbType.VarChar,
                            Direction = ParameterDirection.Input,
                            Value = client_in.Phone
                        };
                        cmd.Parameters.Add(p_phone);

                        SqlParameter p_pass = new SqlParameter
                        {
                            ParameterName = "@Pass",
                            SqlDbType = SqlDbType.VarChar,
                            Direction = ParameterDirection.Input,
                            Value = DbInMemory.CreateMD5(client_in.Pass + DbInMemory.Params.ServiceKey)
                        };
                        cmd.Parameters.Add(p_pass);



                        SqlParameter promo_sum = new SqlParameter
                        {
                            ParameterName = "@Promo_sum",
                            SqlDbType = SqlDbType.BigInt,
                            Direction = ParameterDirection.Input,
                            Value = 0
                        };
                        cmd.Parameters.Add(promo_sum);

                        con.Open();
                        SqlDataReader reader = cmd.ExecuteReader();
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {

                                packet.Result.Code = reader.GetInt32(reader.GetOrdinal("Result")).ToString();
                                packet.Result.Info = reader["Info"].ToString();

                            }
                            if (reader.NextResult())
                            {
                                while (reader.Read())
                                {
                                    client.Uid = reader["Uid"].ToString();
                                    client.FIO = reader["FIO"].ToString();
                                    client.Phone = reader["Phone"].ToString();
                                    client.EMail = reader["EMail"].ToString();
                                    
                                    client.Session = packet.Session = reader["SessionUid"].ToString();
                                    client.ActivationCode = reader["ActivationCode"].ToString();
                                    
                                }
                            }
                            if (reader.NextResult())
                            {
                                while (reader.Read())
                                {
                                    ClientParam p = new ClientParam
                                    {
                                        Ident = reader["Ident"].ToString(),
                                        ValBigInt = reader.IsDBNull(reader.GetOrdinal("ValB")) ? -1 : reader.GetInt64(reader.GetOrdinal("ValB")),
                                        ValDateTime = reader.IsDBNull(reader.GetOrdinal("ValD")) ? DateTime.MinValue : reader.GetDateTime(reader.GetOrdinal("ValD")),
                                        ValString = reader.IsDBNull(reader.GetOrdinal("ValV")) ? "" : reader["ValV"].ToString()
                                    };
                                    client.ClientParams.Add(p);
                                }
                            }
                        }
                        else
                        {
                            reader.Close();
                            con.Close();
                            packet.Result.Code = "K003";
                            packet.Result.Info = "Данные из БД не получены";
                        }
                        reader.Close();
                    }
                }

            }
            catch (Exception ex)
            {
                packet.Result.Code = "K002";
                packet.Result.Info = "Ошибка при обращении к БД";
                Program.Logger.Error(ex, ex.Message);
            }
            packet.Data = client;

            if ((packet.Result.Code == "101" 
                // || packet.Result.Code == "201" // Впоследствии будет необходима доработка с повторной отправкой кода активации.
                )&&(client.Phone.Length>=11))
            {
                // Отправляем СМС сообщение
                try
                {
                    // Пока, так топорно, после надо вынести в таск с занесением в лог БД
                    WebRequest request_sms = WebRequest.Create("https://sms.ru/sms/send?api_id=02BAF581-2DF8-77E3-6C85-4B7CF9ECE4BD&to=" + client.Phone + "&msg=GIDTAXI Ваш пароль " + client_in.Pass + "&json=1&from=GIDTAXI");
                    WebResponse response_sms = request_sms.GetResponse();

                    response_sms.Close();
                }
                catch (Exception ex)
                {
                    Program.Logger.Error(ex.Message);
                }
                client.ActivationCode = "";
            }


            return packet;
        }
           
        
        /// <summary>
        /// Повторная отправка кода активации
        /// </summary>
        /// <param name="client_in"></param>
        /// <returns></returns>
        public static Packet SendActivationCode(Client client_in)
        {
            Packet packet = new Packet();
            packet.Result.Code = "0";
            packet.Result.Info = "OK";

            client_in.Phone = ReturnJustNumbers(client_in.Phone);

            Client client = new Client();

            try
            {
                using (SqlConnection con = new SqlConnection(DbInMemory.Params.ConnectionString))
                {
                    // Авторизуем клиента
                    using (SqlCommand cmd = new SqlCommand("[dbo].[MOB_ClientActivateSend]", con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;


                        SqlParameter p_phone = new SqlParameter
                        {
                            ParameterName = "@p_login",
                            SqlDbType = SqlDbType.VarChar,
                            Direction = ParameterDirection.Input,
                            Value = client_in.Phone
                        };
                        cmd.Parameters.Add(p_phone);


                        con.Open();
                        SqlDataReader reader = cmd.ExecuteReader();
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {

                                packet.Result.Code = reader.GetInt32(reader.GetOrdinal("Result")).ToString();
                                packet.Result.Info = reader["Info"].ToString();

                            }
                            if (reader.NextResult())
                            {
                                while (reader.Read())
                                {
                                    client.Uid = reader["Uid"].ToString();
                                    client.FIO = reader["FIO"].ToString();
                                    client.Phone = reader["Phone"].ToString();
                                    client.EMail = reader["EMail"].ToString();
                                    client.Session = packet.Session = reader["SessionUid"].ToString();
                                    client.ActivationCode = reader["ActivationCode"].ToString();

                                }
                            }

                        }
                        else
                        {
                            reader.Close();
                            con.Close();
                            packet.Result.Code = "K003";
                            packet.Result.Info = "Данные из БД не получены";
                        }
                        reader.Close();
                    }
                }

            }
            catch (Exception ex)
            {
                packet.Result.Code = "K002";
                packet.Result.Info = "Ошибка при обращении к БД";
                Program.Logger.Error(ex, ex.Message);
            }
            packet.Data = null;

            if ((packet.Result.Code == "101"
                // || packet.Result.Code == "201" // Впоследствии будет необходима доработка с повторной отправкой кода активации.
                ) && (client.Phone.Length >= 11))
            {
                // Отправляем СМС сообщение
                try
                {
                    // Пока, так топорно, после надо вынести в таск с занесением в лог БД
                    WebRequest request_sms = WebRequest.Create("https://sms.ru/sms/send?api_id=02BAF581-2DF8-77E3-6C85-4B7CF9ECE4BD&to=" + client.Phone + "&msg=GidTaxi Код активации " + client.ActivationCode + "&json=1");
                    WebResponse response_sms = request_sms.GetResponse();

                    response_sms.Close();
                }
                catch (Exception ex)
                {
                    Program.Logger.Error(ex.Message);
                }
                client.ActivationCode = "";
            }


            return packet;
        }

        
        /// <summary>
        /// Изменение параметров по клиенту
        /// </summary>
        /// <param name="client_in"></param>
        /// <returns></returns>
        public static Packet SetParams(Client client_in, string session)
        {
            Packet packet = new Packet();
            packet.Result.Code = "0";
            packet.Result.Info = "OK";
            client_in.Phone = ReturnJustNumbers(client_in.Phone);
            Client client = new Client();

            try
            {
                // Заполняем параметрами таблицу для передачи в процедуру
                DataTable table = new DataTable();
                table.Columns.Add("Ident", typeof(string));
                table.Columns.Add("ParamV", typeof(string));
                table.Columns.Add("ParamD", typeof(DateTime));
                table.Columns.Add("ParamB", typeof(long));

                foreach(var item in client_in.ClientParams)
                {
                    DataRow row = table.NewRow();
                    row["Ident"] = item.Ident;
                    row["ParamV"] = item.ValString;
                    row["ParamD"] = item.ValDateTime;
                    row["ParamB"] = item.ValBigInt;
                    table.Rows.Add(row);
                }


                using (SqlConnection con = new SqlConnection(DbInMemory.Params.ConnectionString))
                {
                    
                    using (SqlCommand cmd = new SqlCommand("[dbo].[MOB_ClientParams]", con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;


                        SqlParameter p_email = new SqlParameter
                        {
                            ParameterName = "@session",
                            SqlDbType = SqlDbType.VarChar,
                            Direction = ParameterDirection.Input,
                            Value = session
                        };
                        cmd.Parameters.Add(p_email);

                        SqlParameter p_list = new SqlParameter
                        {
                            ParameterName = "@List",
                            SqlDbType = SqlDbType.Structured,
                            Direction = ParameterDirection.Input,
                            Value = table
                        };
                        cmd.Parameters.Add(p_list);

                        con.Open();
                        SqlDataReader reader = cmd.ExecuteReader();
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {

                                packet.Result.Code = reader.GetInt32(reader.GetOrdinal("Result")).ToString();
                                packet.Result.Info = reader["Info"].ToString();

                            }
                            if (reader.NextResult())
                            {
                                while (reader.Read())
                                {
                                    client.Uid = reader["Uid"].ToString();
                                    client.FIO = reader["FIO"].ToString();
                                    client.Phone = reader["Phone"].ToString();
                                    client.EMail = reader["EMail"].ToString();
                                    client.IsCourier = reader.GetBoolean(reader.GetOrdinal("IsCourier"));
                                    client.Session = packet.Session = reader["SessionUid"].ToString();
                                }
                            }
                            if (reader.NextResult())
                            {
                                while (reader.Read())
                                {
                                    ClientParam p = new ClientParam
                                    {
                                        Ident = reader["Ident"].ToString(),
                                        ValBigInt = reader.IsDBNull(reader.GetOrdinal("ValB")) ? -1 : reader.GetInt64(reader.GetOrdinal("ValB")),
                                        ValDateTime = reader.IsDBNull(reader.GetOrdinal("ValD")) ? DateTime.MinValue : reader.GetDateTime(reader.GetOrdinal("ValD")),
                                        ValString = reader.IsDBNull(reader.GetOrdinal("ValV")) ? "" : reader["ValV"].ToString()
                                    };
                                    client.ClientParams.Add(p);
                                }
                            }
                        }
                        else
                        {
                            reader.Close();
                            con.Close();
                            packet.Result.Code = "K003";
                            packet.Result.Info = "Данные из БД не получены";
                        }
                        reader.Close();
                    }
                }

            }
            catch (Exception ex)
            {
                packet.Result.Code = "K002";
                packet.Result.Info = "Ошибка при обращении к БД";
                Program.Logger.Error(ex, ex.Message);
            }
            packet.Data = client;
            return packet;
        }

        
        /// <summary>
        /// Разлогинивание клиента
        /// </summary>
        /// <param name="client_in"></param>
        /// <returns></returns>
        public static Packet LogoutClient(string SessionId)
        {
            Packet packet = new Packet();
            packet.Result.Code = "0";
            packet.Result.Info = "OK";

            Client client = new Client();

            try
            {
                using (SqlConnection con = new SqlConnection(DbInMemory.Params.ConnectionString))
                {
                    // Авторизуем клиента
                    using (SqlCommand cmd = new SqlCommand("[dbo].[MOB_ClientLogout]", con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;

                        SqlParameter p_session = new SqlParameter
                        {
                            ParameterName = "@session",
                            SqlDbType = SqlDbType.VarChar,
                            Direction = ParameterDirection.Input,
                            Value = SessionId.ToUpper()
                        };
                        cmd.Parameters.Add(p_session);

                        con.Open();
                        SqlDataReader reader = cmd.ExecuteReader();
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {

                                packet.Result.Code = reader.GetInt32(reader.GetOrdinal("Result")).ToString();
                                packet.Result.Info = reader["Info"].ToString();

                            }
                        }
                        else
                        {
                            reader.Close();
                            con.Close();
                            packet.Result.Code = "K003";
                            packet.Result.Info = "Данные из БД не получены";
                        }
                        reader.Close();
                    }
                }

            }
            catch (Exception ex)
            {
                packet.Result.Code = "K002";
                packet.Result.Info = "Ошибка при обращении к БД";
                Program.Logger.Error(ex, ex.Message);
            }
            packet.Data = client;
            return packet;
        }




        /// <summary>
        /// Разлогинивание клиента
        /// </summary>
        /// <param name="client_in"></param>
        /// <returns></returns>
        public static Packet SetLang(string SessionId, long LangId)
        {
            Packet packet = new Packet();
            packet.Result.Code = "0";
            packet.Result.Info = "OK";

            

            try
            {
                using (SqlConnection con = new SqlConnection(DbInMemory.Params.ConnectionString))
                {
                    // Меняем язык
                    using (SqlCommand cmd = new SqlCommand("[dbo].[MOB_ClientSetLang]", con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;

                        SqlParameter p_lang = new SqlParameter
                        {
                            ParameterName = "@p_Lang",
                            SqlDbType = SqlDbType.BigInt,
                            Direction = ParameterDirection.Input,
                            Value = LangId
                        };
                        cmd.Parameters.Add(p_lang);

                        SqlParameter p_session = new SqlParameter
                        {
                            ParameterName = "@p_session",
                            SqlDbType = SqlDbType.VarChar,
                            Direction = ParameterDirection.Input,
                            Value = SessionId.ToUpper()
                        };
                        cmd.Parameters.Add(p_session);

                        con.Open();
                        SqlDataReader reader = cmd.ExecuteReader();
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {

                                packet.Result.Code = reader["Code"].ToString();
                                packet.Result.Info = reader["Info"].ToString();

                            }
                        }
                        else
                        {
                            reader.Close();
                            con.Close();
                            packet.Result.Code = "K003";
                            packet.Result.Info = "Данные из БД не получены";
                        }
                        reader.Close();
                    }
                }

            }
            catch (Exception ex)
            {
                packet.Result.Code = "K002";
                packet.Result.Info = "Ошибка при обращении к БД";
                Program.Logger.Error(ex, ex.Message);
            }
            
            return packet;
        }


        /// <summary>
        /// Разлогинивание клиента
        /// </summary>
        /// <param name="client_in"></param>
        /// <returns></returns>
        public static Packet GetLangAll(string SessionId)
        {
            Packet packet = new Packet();
            packet.Result.Code = "0";
            packet.Result.Info = "OK";

            List<Lang> langs = new List<Lang>();

            try
            {
                using (SqlConnection con = new SqlConnection(DbInMemory.Params.ConnectionString))
                {
                    // Меняем язык
                    using (SqlCommand cmd = new SqlCommand("[dbo].[MOB_Lang_GetAll]", con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;

                        SqlParameter p_session = new SqlParameter
                        {
                            ParameterName = "@p_session",
                            SqlDbType = SqlDbType.VarChar,
                            Direction = ParameterDirection.Input,
                            Value = SessionId.ToUpper()
                        };
                        cmd.Parameters.Add(p_session);

                        con.Open();
                        SqlDataReader reader = cmd.ExecuteReader();
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {

                                packet.Result.Code = reader["Code"].ToString();
                                packet.Result.Info = reader["Info"].ToString();

                            }
                            if (reader.NextResult())
                            {
                                while (reader.Read())
                                {
                                    Lang lang = new Lang
                                    {
                                        Id = (int)reader.GetInt64(reader.GetOrdinal("Id")),
                                        Ident = reader["ident"].ToString(),
                                        Name = reader["Name"].ToString()
                                    };

                                    langs.Add(lang);
                                }
                            }
                        }
                        else
                        {
                            reader.Close();
                            con.Close();
                            packet.Result.Code = "K003";
                            packet.Result.Info = "Данные из БД не получены";
                        }
                        reader.Close();
                    }
                }

            }
            catch (Exception ex)
            {
                packet.Result.Code = "K002";
                packet.Result.Info = "Ошибка при обращении к БД";
                Program.Logger.Error(ex, ex.Message);
            }

            packet.Data = langs;

            return packet;
        }


        public static (string, bool) GetClientsPhone(string session)
        {
            string phone = "";
            bool authorized = false;
            try
            {
                using (SqlConnection con = new SqlConnection(DbInMemory.Params.ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("[dbo].[MOB_GetClientsPhone]", con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        SqlParameter p_session = new SqlParameter
                        {
                            ParameterName = "@p_session",
                            SqlDbType = SqlDbType.VarChar,
                            Direction = ParameterDirection.Input,
                            Value = session
                        };
                        cmd.Parameters.Add(p_session);

                        con.Open();
                        SqlDataReader reader = cmd.ExecuteReader();
                        if(reader.HasRows)
                        {
                            while(reader.Read())
                            {
                                phone = reader["Phone"].ToString();
                                authorized = reader.GetBoolean(reader.GetOrdinal("Auth"));
                            }
                        }
                        else
                        {
                            con.Close();
                        }
                        reader.Close();
                    }
                }
            }
            catch(Exception ex)
            {
                Program.Logger.Error(ex, ex.Message);
            }
            return (phone, authorized);
        }


    }
}