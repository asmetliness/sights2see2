using System.Data;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
using System;
using System.Net;
using System.Collections.Generic;
using GidTaxiApi.Classes.Models.CheckPoints;
using GidTaxiApi.Classes.Models.DataExchange;
using System.Threading.Tasks;

namespace GidTaxiApi.Classes.Data.DBRepository
{
    public class DBCheckPoints
    {
        public static async Task<CheckPoint> GetCheckPoint(string checkPointUid, long lang)
        {
            CheckPoint checkPoint = null;
            try
            {
                using (SqlConnection con = new SqlConnection(DbInMemory.Params.ConnectionString))
                {
                    using (SqlCommand cmd = new SqlCommand("[dbo].[MOB_CheckPoint_Get]", con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;

                        SqlParameter p_checkPointUid = new SqlParameter
                        {
                            ParameterName = "@p_checkPointUid",
                            SqlDbType = SqlDbType.VarChar,
                            Direction = ParameterDirection.Input,
                            Value = checkPointUid
                        };
                        cmd.Parameters.Add(p_checkPointUid);


                        SqlParameter p_langId = new SqlParameter
                        {
                            ParameterName = "@p_lang",
                            SqlDbType = SqlDbType.BigInt,
                            Direction = ParameterDirection.Input,
                            Value = lang
                        };
                        cmd.Parameters.Add(p_langId);

                        await con.OpenAsync();
                        SqlDataReader reader = await cmd.ExecuteReaderAsync();
                        if (reader.HasRows)
                        {

                                while (await reader.ReadAsync())
                                {
                                    CheckPoint cp = new CheckPoint();
                                    cp.Id = reader.GetInt64(reader.GetOrdinal("Id"));
                                    cp.Uid = reader["Uid"].ToString();
                                    cp.Name = reader["Name"].ToString();
                                    cp.Order = reader.GetInt64(reader.GetOrdinal("Order"));
                                    cp.Active = reader.GetBoolean(reader.GetOrdinal("Active"));
                                    cp.DateCreate = reader.GetDateTime(reader.GetOrdinal("DateCreate"));
                                    cp.DateEdit = reader.GetDateTime(reader.GetOrdinal("DateEdit"));
                                    cp.Rating = reader.GetDecimal(reader.GetOrdinal("Rating"));
                                    cp.CheckPointType = new CheckPointType
                                    {
                                        Id = reader.GetInt32(reader.GetOrdinal("CheckPointType"))
                                    };
                                    cp.Address = new Address();
                                    cp.Address.Gps = reader["GPS"].ToString();
                                    cp.Address.Country = reader["AdrCountry"].ToString();
                                    cp.Address.City = reader["AdrCity"].ToString();
                                    cp.Address.Street = reader["AdrStreet"].ToString();
                                    cp.Address.House = reader["AdrHouse"].ToString();
                                    cp.Address.Radius = reader["AdrRadius"].ToString();
                                    cp.WorkDays = reader["WorkDays"].ToString();
                                    cp.WorkHours = reader["WorkHours"].ToString();
                                    cp.MainImgFileName = reader["MainImgFileName"].ToString();
                                    cp.Description = reader["Description"].ToString();
                                    cp.HashTags = reader["HashTags"].ToString();
                                    cp.Audio = reader["Audio"].ToString();
                                    checkPoint = cp;
                                }
                            

                        }
                        else
                        {
                            con.Close();
                        }
                        reader.Close();
                    }
                }
            }
            catch (Exception ex)
            {
                Program.Logger.Error(ex, ex.Message);
            }
            return checkPoint;
        }


        /// <summary>
        /// Получение списка всех достопримечательностей
        /// </summary>
        /// <returns></returns>
        public static async Task<List<CheckPoint>> GetCheckPointAll(long lang, int page)
        {
            List<CheckPoint> checkPoints = new List<CheckPoint>();
            try
            {
                using (SqlConnection con = new SqlConnection(DbInMemory.Params.ConnectionString))
                {

                    using (SqlCommand cmd = new SqlCommand("[dbo].[MOB_CheckPoint_GetAll]", con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;

                        cmd.Parameters.Add(new SqlParameter
                        {
                            ParameterName = "@p_lang",
                            Direction = ParameterDirection.Input,
                            Value = lang,
                            SqlDbType = SqlDbType.BigInt
                        });

                        cmd.Parameters.Add(new SqlParameter
                        {
                            ParameterName = "@p_page",
                            Direction = ParameterDirection.Input,
                            Value = page,
                            SqlDbType = SqlDbType.Int
                        });

                        await con.OpenAsync();
                        SqlDataReader reader = await cmd.ExecuteReaderAsync();
                        if (reader.HasRows)
                        {

                            while (await reader.ReadAsync())
                            {
                                CheckPoint cp = new CheckPoint
                                {
                                    Id = reader.GetInt64(reader.GetOrdinal("Id")),
                                    Uid = reader["Uid"].ToString(),
                                    Name = reader["Name"].ToString(),
                                    Order = reader.GetInt64(reader.GetOrdinal("Order")),
                                    Active = reader.GetBoolean(reader.GetOrdinal("Active")),
                                    DateCreate = reader.GetDateTime(reader.GetOrdinal("DateCreate")),
                                    DateEdit = reader.GetDateTime(reader.GetOrdinal("DateEdit")),
                                    Rating = reader.GetDecimal(reader.GetOrdinal("Rating")),
                                    CheckPointType = new CheckPointType
                                    {
                                        Id = reader.GetInt32(reader.GetOrdinal("CheckPointType")),
                                        Name = reader["TypeName"].ToString()
                                    },
                                    Address = new Address
                                    {
                                        Gps = reader["GPS"].ToString(),
                                        Country = reader["AdrCountry"].ToString(),
                                        City = reader["AdrCity"].ToString(),
                                        Street = reader["AdrStreet"].ToString(),
                                        House = reader["AdrHouse"].ToString(),
                                        Radius = reader["AdrRadius"].ToString()
                                    },
                                    WorkDays = reader["WorkDays"].ToString(),
                                    WorkHours = reader["WorkHours"].ToString(),
                                    MainImgFileName = reader["MainImgFileName"].ToString(),
                                    Description = reader["Description"].ToString(),
                                    HashTags = reader["HashTags"].ToString(),
                                    Audio = reader["Audio"].ToString()
                                };
                                checkPoints.Add(cp);
                            }


                        }
                        else
                        {
                            con.Close();
                        }
                        reader.Close();
                    }
                }

            }
            catch (Exception ex)
            {
                Program.Logger.Error(ex, ex.Message);
            }
            return checkPoints;
        }

   





    }
}