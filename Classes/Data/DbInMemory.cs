using System.Text;
using System.Linq;
namespace GidTaxiApi.Classes.Data
{
    /// <summary>
    /// Статические данные подгружаемые используемые всеми компонентами приложения
    /// </summary>
    public static class DbInMemory
    {
        /// <summary>
        /// Параметры настроек службы, подгружаемые при старте
        /// </summary>
        public static class Params
        {

            /// <summary>
            /// Строка подключения к БД
            /// </summary>
            public static string ConnectionString { get; set; }
            
            /// <summary>
            /// Сервисный ключ для хеширования
            /// </summary>
            public static string ServiceKey { get; set; }
            


            /// <summary>
            /// Путь до папки где картинки лежат
            /// </summary>
            public static string ImgPath { get; set; }
            
            /// <summary>
            /// Путь до папки где аудиотреки лежат
            /// </summary>
            public static string AudioPath { get; set; }

        }
        
        
        public static string CreateMD5(string input)
        {
            // Use input string to calculate MD5 hash
            using (System.Security.Cryptography.MD5 md5 = System.Security.Cryptography.MD5.Create())
            {
                byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
                byte[] hashBytes = md5.ComputeHash(inputBytes);

                // Convert the byte array to hexadecimal string
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < hashBytes.Length; i++)
                {
                    sb.Append(hashBytes[i].ToString("X2"));
                }
                return sb.ToString();
            }
        }

        public static string CreateMD5_2(string input)
        {
            // Use input string to calculate MD5 hash
            using (System.Security.Cryptography.MD5 md5 = System.Security.Cryptography.MD5.Create())
            {
                byte[] inputBytes = System.Text.Encoding.UTF8.GetBytes(input);
                byte[] hashBytes = md5.ComputeHash(inputBytes);
                // Convert the byte array to hexadecimal string
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < hashBytes.Length; i++)
                {
                    
                    sb.Append(hashBytes[i].ToString("X2"));
                }
                return sb.ToString();
            }
        }

       
    }
    
    
    
}