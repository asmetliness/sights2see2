﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GidTaxiApi.Classes.Data.DBRepository;
using GidTaxiApi.Classes.Models.CheckPoints;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Sights.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CheckPointController : ControllerBase
    {

        [HttpGet]
        [Route("list")]
        public async Task<ActionResult<List<CheckPoint>>> GetCheckPoints([FromQuery]long lang = 2, [FromQuery]int page = 1)
        {
            return await DBCheckPoints.GetCheckPointAll(lang, page);
        }

        [HttpGet]
        public async Task<ActionResult<CheckPoint>> GetOne([FromQuery]string uid, [FromQuery]long lang = 2)
        {
            var result = await DBCheckPoints.GetCheckPoint(uid, lang);
            if(result == null)
            {
                return NotFound();
            }
            return result;
        }

    }
}