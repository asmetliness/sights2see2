﻿using System;
using Microsoft.AspNetCore.Mvc;
using System.IO;
using GidTaxiApi.Classes.Data;
using GidTaxiApi.Classes.Data.DBRepository;

namespace GidTaxiApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FileController : ControllerBase
    {
        [HttpGet]
        public ActionResult Get(string session, string file)
        {
            Program.Logger.Info("!!! File Request " + session + " File: " + file);

                if (file == null)
                    return Content("filename not present");
                try
                {
                    var path = Path.Combine(DbInMemory.Params.AudioPath, file);

                    var memory = new MemoryStream();
                    using (var stream = new FileStream(path, FileMode.Open))
                    {
                        stream.CopyTo(memory);
                    }

                    memory.Position = 0;
                    return File(memory, "audio/mpeg", Path.GetFileName(path));
                }
                catch(Exception ex)
                {
                    Program.Logger.Error(ex, ex.Message);
                }
                return NotFound();

        }
    }

}