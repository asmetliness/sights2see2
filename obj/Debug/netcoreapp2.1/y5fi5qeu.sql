﻿IF OBJECT_ID(N'[__EFMigrationsHistory]') IS NULL
BEGIN
    CREATE TABLE [__EFMigrationsHistory] (
        [MigrationId] nvarchar(150) NOT NULL,
        [ProductVersion] nvarchar(32) NOT NULL,
        CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY ([MigrationId])
    );
END;

GO

CREATE SEQUENCE [SequenceCheckPointId] START WITH 1 INCREMENT BY 1 NO MINVALUE NO MAXVALUE NO CYCLE;

GO

CREATE TABLE [Lang] (
    [Id] bigint NOT NULL,
    [Ident] varchar(50) NOT NULL,
    [Name] varchar(250) NOT NULL,
    CONSTRAINT [PK_Lang] PRIMARY KEY ([Id])
);

GO

CREATE TABLE [Params] (
    [Uid] uniqueidentifier NOT NULL,
    [Param] varchar(max) NOT NULL,
    [Value] varchar(max) NOT NULL,
    [DT] datetime NOT NULL,
    [Info] varchar(max) NOT NULL,
    CONSTRAINT [PK_Params] PRIMARY KEY ([Uid])
);

GO

CREATE TABLE [CheckPoint] (
    [Id] bigint NOT NULL,
    [Uid] uniqueidentifier NOT NULL,
    [GPS] varchar(50) NOT NULL,
    [DateCreate] datetime NOT NULL,
    [DateEdit] datetime NOT NULL,
    [Rating] decimal(18, 2) NOT NULL,
    [TypeId] bigint NULL,
    [MainImgFileName] varchar(max) NULL,
    [WorkDays] varchar(7) NULL,
    [WorkHours] varchar(24) NULL,
    [Radius] varchar(max) NULL,
    CONSTRAINT [PK_CheckPoint] PRIMARY KEY ([Id]),
    CONSTRAINT [FK_CheckPoint_CheckPointType_TypeId] FOREIGN KEY ([TypeId]) REFERENCES [CheckPointType] ([Id]) ON DELETE NO ACTION
);

GO

CREATE TABLE [CheckPointType_Lang] (
    [Uid] uniqueidentifier NOT NULL,
    [LangId] bigint NULL,
    [Name] varchar(250) NOT NULL,
    [CheckPointTypeId] bigint NULL,
    CONSTRAINT [PK_CheckPointType_Lang] PRIMARY KEY ([Uid]),
    CONSTRAINT [FK_CheckPointType_Lang_CheckPointType_CheckPointTypeId] FOREIGN KEY ([CheckPointTypeId]) REFERENCES [CheckPointType] ([Id]) ON DELETE NO ACTION,
    CONSTRAINT [FK_CheckPointType_Lang_Lang_LangId] FOREIGN KEY ([LangId]) REFERENCES [Lang] ([Id]) ON DELETE NO ACTION
);

GO

CREATE TABLE [Clients] (
    [Uid] uniqueidentifier NOT NULL,
    [FIO] varchar(250) NOT NULL,
    [EMail] varchar(250) NOT NULL,
    [Phone] varchar(50) NOT NULL,
    [Pass] varchar(50) NOT NULL,
    [DateCreate] datetime NOT NULL,
    [SessionUid] varchar(36) NOT NULL,
    [LangId] bigint NULL,
    CONSTRAINT [PK_Clients] PRIMARY KEY ([Uid]),
    CONSTRAINT [FK_Clients_Lang_LangId] FOREIGN KEY ([LangId]) REFERENCES [Lang] ([Id]) ON DELETE NO ACTION
);

GO

CREATE TABLE [CheckPoint_Lang] (
    [Uid] uniqueidentifier NOT NULL,
    [LangId] bigint NULL,
    [Name] varchar(250) NOT NULL,
    [Description] varchar(max) NOT NULL,
    [AdrCountry] varchar(150) NULL,
    [AdrCity] varchar(150) NULL,
    [AdrStreet] varchar(150) NULL,
    [AdrHouse] varchar(150) NULL,
    [Audio] varchar(max) NULL,
    [CheckPointId] bigint NULL,
    CONSTRAINT [PK_CheckPoint_Lang] PRIMARY KEY ([Uid]),
    CONSTRAINT [FK_CheckPoint_Lang_CheckPoint_CheckPointId] FOREIGN KEY ([CheckPointId]) REFERENCES [CheckPoint] ([Id]) ON DELETE NO ACTION,
    CONSTRAINT [FK_CheckPoint_Lang_Lang_LangId] FOREIGN KEY ([LangId]) REFERENCES [Lang] ([Id]) ON DELETE NO ACTION
);

GO

CREATE TABLE [FeedBack] (
    [Uid] uniqueidentifier NOT NULL,
    [Comment] varchar(max) NULL,
    [Rate] int NULL,
    [DateCreate] datetime NULL DEFAULT ((getdate())),
    [LangId] bigint NULL,
    [CheckPointId] bigint NULL,
    CONSTRAINT [PK_FeedBack] PRIMARY KEY ([Uid]),
    CONSTRAINT [FK_FeedBack_CheckPoint_CheckPointId] FOREIGN KEY ([CheckPointId]) REFERENCES [CheckPoint] ([Id]) ON DELETE NO ACTION,
    CONSTRAINT [FK_FeedBack_Lang_LangId] FOREIGN KEY ([LangId]) REFERENCES [Lang] ([Id]) ON DELETE NO ACTION
);

GO

CREATE INDEX [IX_CheckPoint_TypeId] ON [CheckPoint] ([TypeId]);

GO

CREATE INDEX [IX_CheckPoint] ON [CheckPoint] ([Id], [DateCreate], [DateEdit], [GPS], [Rating], [Uid]);

GO

CREATE INDEX [IX_CheckPoint_Lang_CheckPointId] ON [CheckPoint_Lang] ([CheckPointId]);

GO

CREATE INDEX [IX_CheckPoint_Lang_LangId] ON [CheckPoint_Lang] ([LangId]);

GO

CREATE INDEX [IX_CheckPointType_Lang_CheckPointTypeId] ON [CheckPointType_Lang] ([CheckPointTypeId]);

GO

CREATE INDEX [IX_CheckPointType_Lang_LangId] ON [CheckPointType_Lang] ([LangId]);

GO

CREATE INDEX [IX_Clients_LangId] ON [Clients] ([LangId]);

GO

CREATE INDEX [IX_FeedBack_CheckPointId] ON [FeedBack] ([CheckPointId]);

GO

CREATE INDEX [IX_FeedBack_LangId] ON [FeedBack] ([LangId]);

GO

CREATE INDEX [IX_Lang] ON [Lang] ([Id], [Ident], [Name]);

GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20190511095301_initial', N'2.1.8-servicing-32085');

GO

DROP SEQUENCE [SequenceCheckPointId];

GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20190511100721_Test', N'2.1.8-servicing-32085');

GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20190511100948_Test1', N'2.1.8-servicing-32085');

GO

