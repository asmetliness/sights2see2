﻿IF OBJECT_ID(N'[__EFMigrationsHistory]') IS NULL
BEGIN
    CREATE TABLE [__EFMigrationsHistory] (
        [MigrationId] nvarchar(150) NOT NULL,
        [ProductVersion] nvarchar(32) NOT NULL,
        CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY ([MigrationId])
    );
END;

GO

CREATE TABLE [CheckPoint] (
    [Id] bigint NOT NULL,
    [Uid] uniqueidentifier NOT NULL,
    [Order] bigint NOT NULL,
    [GPS] varchar(50) NOT NULL,
    [Active] bit NOT NULL,
    [DateCreate] datetime NOT NULL,
    [DateEdit] datetime NOT NULL,
    [Rating] decimal(18, 2) NOT NULL,
    [CheckPointType] int NOT NULL,
    [MainImgFileName] varchar(max) NULL,
    [WorkDays] varchar(7) NULL,
    [WorkHours] varchar(24) NULL,
    [Audio] varchar(max) NULL,
    [GPS1] decimal(18, 6) NULL,
    [GPS2] decimal(18, 6) NULL,
    [Radius] varchar(max) NULL,
    [test] nvarchar(max) NULL,
    CONSTRAINT [PK_CheckPoint] PRIMARY KEY ([Id])
);

GO

CREATE TABLE [CheckPoint_Lang] (
    [Uid] uniqueidentifier NOT NULL,
    [CheckPointUid] uniqueidentifier NOT NULL,
    [Lang] int NOT NULL,
    [Name] varchar(250) NOT NULL,
    [Description] varchar(max) NOT NULL,
    [AdrCountry] varchar(150) NULL,
    [AdrCity] varchar(150) NULL,
    [AdrStreet] varchar(150) NULL,
    [AdrHouse] varchar(150) NULL,
    [Audio] varchar(max) NULL,
    CONSTRAINT [PK_CheckPoint_Lang] PRIMARY KEY ([Uid])
);

GO

CREATE TABLE [CheckPointType] (
    [Id] bigint NOT NULL,
    CONSTRAINT [PK_CheckPointType] PRIMARY KEY ([Id])
);

GO

CREATE TABLE [CheckPointType_Lang] (
    [Uid] uniqueidentifier NOT NULL,
    [ChckPointTypeId] bigint NOT NULL,
    [Lang] int NOT NULL,
    [Name] varchar(250) NOT NULL,
    CONSTRAINT [PK_CheckPointType_Lang] PRIMARY KEY ([Uid])
);

GO

CREATE TABLE [Clients] (
    [Uid] uniqueidentifier NOT NULL,
    [FIO] varchar(250) NOT NULL,
    [EMail] varchar(250) NOT NULL,
    [Phone] varchar(50) NOT NULL,
    [Pass] varchar(50) NOT NULL,
    [DateCreate] datetime NOT NULL,
    [SessionUid] varchar(36) NOT NULL,
    [LangId] bigint NULL,
    CONSTRAINT [PK_Clients] PRIMARY KEY ([Uid])
);

GO

CREATE TABLE [FeedBack] (
    [Uid] uniqueidentifier NOT NULL,
    [ClientUid] uniqueidentifier NULL,
    [Comment] varchar(max) NULL,
    [Rate] int NULL,
    [DateCreate] datetime NULL DEFAULT ((getdate())),
    [Lang] varchar(max) NULL,
    [RouteUid] uniqueidentifier NULL,
    [CheckPointUid] uniqueidentifier NULL,
    [status] bit NULL DEFAULT (((1))),
    [Type] int NULL DEFAULT (((1))),
    [Phone] varchar(max) NULL,
    [LangId] bigint NULL,
    CONSTRAINT [PK_FeedBack] PRIMARY KEY ([Uid])
);

GO

CREATE TABLE [FeedBack_Type] (
    [Id] int NOT NULL,
    [Description] varchar(max) NULL,
    CONSTRAINT [PK_FeedBack_Type] PRIMARY KEY ([Id])
);

GO

CREATE TABLE [Lang] (
    [Id] bigint NOT NULL,
    [Ident] varchar(50) NOT NULL,
    [Name] varchar(250) NOT NULL,
    CONSTRAINT [PK_Lang] PRIMARY KEY ([Id])
);

GO

CREATE TABLE [Params] (
    [Uid] uniqueidentifier NOT NULL,
    [Param] varchar(max) NOT NULL,
    [Value] varchar(max) NOT NULL,
    [DT] datetime NOT NULL,
    [Info] varchar(max) NOT NULL,
    CONSTRAINT [PK_Params] PRIMARY KEY ([Uid])
);

GO

CREATE INDEX [IX_CheckPoint] ON [CheckPoint] ([Id], [Active], [CheckPointType], [DateCreate], [DateEdit], [GPS], [Order], [Rating], [Uid]);

GO

CREATE INDEX [IX_CheckPointType_Lang] ON [CheckPointType_Lang] ([Uid], [ChckPointTypeId], [Lang], [Name]);

GO

CREATE INDEX [IX_Lang] ON [Lang] ([Id], [Ident], [Name]);

GO

INSERT INTO [__EFMigrationsHistory] ([MigrationId], [ProductVersion])
VALUES (N'20190511103514_Initial', N'2.1.8-servicing-32085');

GO

